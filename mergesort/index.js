export const mergesort = (vals) => {
  // Terminal case
  if (vals.length <= 1) return vals;

  // Grab the pivot index (center)
  const pivot = Math.floor(vals.length / 2);

  // Recurse down
  let left = mergesort(vals.slice(0, pivot));
  let right = mergesort(vals.slice(pivot));

  // Merge left and right arrays
  const merged = [];
  while (left.length > 0 || right.length > 0) {
    if (left.length === 0) merged.push(right.shift());
    else if (right.length === 0) merged.push(left.shift());
    else merged.push(left[0] <= right[0] ? left.shift() : right.shift());
  }
  return merged;
};

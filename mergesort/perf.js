import { mergesort } from './';

import { checkSort } from '../util';

const arr = [];
for (let i = 0; i < 10000; i++) {
  arr.push(Math.floor(Math.random() * 10000));
}

const t0 = console.time('Mergesort (random)');

const sorted = mergesort(arr);

const t1 = console.timeEnd('Mergesort (random)');

console.log(`Sort asc correct: ${checkSort(sorted)}`);

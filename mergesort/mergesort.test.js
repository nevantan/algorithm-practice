import { mergesort } from './';

describe('the mergesort algorithm', () => {
  it('should sort values in ascending order', () => {
    const arr = [4, 2, 5, 1, 3, 6, 9, 8, 7];
    const sorted = mergesort(arr);
    expect(sorted).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });
});

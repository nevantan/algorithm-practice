import { quicksort } from './';

describe('the quicksort method', () => {
  it('should return values in ascending order', () => {
    const arr = [5, 3, 4, 2, 1];
    const sorted = quicksort(arr);
    expect(sorted).toEqual([1, 2, 3, 4, 5]);
  });
});

import { quicksort } from './';

import { checkSort } from '../util';

const arr = [];
for (let i = 0; i < 10000; i++) {
  arr.push(Math.floor(Math.random() * 10000));
}

const t0 = console.time('Quicksort (random)');

const sorted = quicksort(arr);

const t1 = console.timeEnd('Quicksort (random)');

console.log(`Sort asc correct: ${checkSort(sorted)}`);

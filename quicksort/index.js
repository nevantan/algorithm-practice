export const quicksort = (vals) => {
  if (vals.length <= 1) return vals;

  const pivot = Math.floor(vals.length / 2);
  const pval = vals[pivot];

  const low = vals.filter((val, i) => i !== pivot && val <= pval);
  const high = vals.filter((val, i) => i !== pivot && val > pval);

  return [...quicksort(low), pval, ...quicksort(high)];
};

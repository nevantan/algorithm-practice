import { gen } from './';

describe('the fizzbuzz function', () => {
  it('should return correct values in sequence', () => {
    expect(gen.next().value).toBe(1);
    expect(gen.next().value).toBe(2);
    expect(gen.next().value).toBe('Fizz');
    expect(gen.next().value).toBe(4);
    expect(gen.next().value).toBe('Buzz');
    expect(gen.next().value).toBe('Fizz');
    expect(gen.next().value).toBe(7);
    expect(gen.next().value).toBe(8);
    expect(gen.next().value).toBe('Fizz');
    expect(gen.next().value).toBe('Buzz');
    expect(gen.next().value).toBe(11);
    expect(gen.next().value).toBe('Fizz');
    expect(gen.next().value).toBe(13);
    expect(gen.next().value).toBe(14);
    expect(gen.next().value).toBe('FizzBuzz');
  });
});

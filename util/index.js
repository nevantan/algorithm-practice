export const checkSort = (arr) => {
  for (let i = 0; i < arr - 1; i++) {
    if (arr[i] > arr[i + 1]) return false;
  }
  return true;
};
